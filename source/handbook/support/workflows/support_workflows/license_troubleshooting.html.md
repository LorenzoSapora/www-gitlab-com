---
layout: markdown_page
title: License Troubleshooting
category: Support Workflows
---

## On This Page
{:.no_toc}

- TOC
{:toc}

----
## Overview
When a customer is having problems with a licensing on self-hosted or .com

## GitLab Self Hosted Subscriptions

When a customer reports problems when registering their license key please check the points below in order to diagnose the problem:

1. How many active users are in the GitLab instance of the customer? Does their license key supports that number of active users?

    This is the main issue that we've noted when receiving requests related to problems with registering a license.

    You can ask the customer the following:

    > How many active users do you have? (You can find this on the admin dashboard at `http://<hostname>/admin/users` next to the "Active" tab)

1. If the customer reports the following validation error:

    > During the year before this license started, this GitLab installation had 0 active users, exceeding this license's limit of 5 by -5 users. Please upload a license for at least 0 users or contact sales at renewals@gitlab.com

    That usually means that that GitLab instance has more active users than the number allowed by the license key. Please ask the customer for the number of active users and suggest to buy more seats if required.

    That validation error [has been fixed](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/4961) but the fix will be included in the `10.7` version of GitLab.

1.  If the customer reports the following validation error:

    ![Screen_Shot_2018-03-07_at_5.43.18_PM](/uploads/035d53fa5f176010d3c73a5a5550d18c/Screen_Shot_2018-03-07_at_5.43.18_PM.png)

    That usually means that the number of Trueup purchased is invalid. If you consider that the customer has purchased the adequate number of Trueup please follow these steps:

    1. Go to the [license app](https://license.gitlab.com/)
    2. Find the last license key by email or company name
    3. Duplicate the license key
    4. Edit the `Trueup count` field with the correct number
    5. Save the license key


1. If you can't solve the problem with any of the above steps please contact to `@ruben`.

## GitLab.com Subscriptions

In order to purchase a GitLab.com subscription, we need the customer to have an account on [GitLab.com](https://gitlab.com/users/sign_in), please ask the customer to create an account first in case they don't have one yet.

If the customer can't see their Groups when purchasing a subscription please let them now that they need to create the Groups on [GitLab.com](https://gitlab.com/users/sign_in) first.

For other type of problems please contact `@ruben`.