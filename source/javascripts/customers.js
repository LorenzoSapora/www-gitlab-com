/* eslint-disable no-undef, no-new, no-eq-null, eqeqeq */

(function() {
  var ROW_SIZE = 4;
  var ROWS_PER_PAGE = 4;

  var sortPagination = function(total, page) {
    var items = [];
    if (page > 1) items.push({ title: '<<', where: 1 });
    if (page > 1) {
      items.push({ title: '<', where: page - 1 });
    }

    if (page > 6) items.push({ title: '...', separator: true });

    var start = Math.max(page - 4, 1);
    var end = Math.min(page + 4, total);

    for (var i = start; i <= end; i++) {
      var isActive = i === page;
      items.push({ title: i, active: isActive, where: i });
    }

    if (total - page > 4) items.push({ title: '...', separator: true });

    if (total - page >= 1) {
      items.push({ title: '>', where: page + 1 });
    }

    if (total - page >= 1) items.push({ title: '>>', where: total });

    return items;
  };

  var addClickListenerCtaButton = function($ctaButton) {
    $ctaButton.on('click', function() {
      var url = 'free-trial/';
      var customerEmail = $('input[name="customer-email"]').val();
      url = url + '?email=' + customerEmail;
      window.location = url;
    });
  };


  this.CustomerListHandler = (function() {
    function CustomerListHandler() {
      this.$filters = $('input[name=filter-industry]:radio', '.filter-list');
      this.$orgList = $('.org-image', '.org-groups-container');
      this.$orgContainer = $('.org-groups-container');
      $(this.$filters[0]).attr('checked', 'checked');

      // binds
      this.$filters.on('click', this.render.bind(this));
      this.render.call(this);
    }

    CustomerListHandler.prototype.filterData = function() {
      var $filterValue = $('input[name=filter-industry]:checked', '.filter-list').val();

      var filteredData = this.$orgList.filter(function() {
        if ($filterValue === 'any') {
          return true;
        } else if ($filterValue === $(this).data('industry-type')) {
          return true;
        }
        return false;
      });

      this.groupedFilteredData = [];

      while (filteredData.length > 0) {
        this.groupedFilteredData.push(filteredData.splice(0, ROW_SIZE));
      }
    };

    CustomerListHandler.prototype.render = function(e) {
      this.filterData.call(this);
      function renderData(pageNumber) {
        if (!pageNumber) {
          this.currentPage = 1;
          this.renderOrgList.call(this);
        } else {
          var newPageNumber;
          if (pageNumber.text().toLowerCase() === '>') {
            newPageNumber = this.currentPage += 1;
          } else if (pageNumber.text().toLowerCase() === '<') {
            newPageNumber = this.currentPage -= 1;
          } else if (pageNumber.text().toLowerCase().indexOf('<<') !== -1) {
            newPageNumber = this.currentPage = 1;
          } else if (pageNumber.text().toLowerCase().indexOf('>>') !== -1) {
            newPageNumber = this.getTotalPages();
          } else {
            newPageNumber = parseInt(pageNumber.text(), 10);
          }
          this.currentPage = newPageNumber;
          this.renderOrgList.call(this);
        }
      }
      if (e != null) {
        var $callingElement = $(e.currentTarget);
        if ($callingElement.attr('type') !== 'radio') {
          renderData.call(this, $callingElement);
        } else {
          renderData.call(this);
        }
      } else {
        renderData.call(this);
      }
    };

    CustomerListHandler.prototype.renderOrgList = function() {
      this.$orgContainer.empty();
      $('.pagination li span').off('click');
      document.querySelector('.pagination').innerHTML =
      sortPagination(this.getTotalPages(), this.currentPage)
        .map(function(e) {
          var activeElement;
          if (e.active) {
            activeElement = '<li seperator="' + e.seperator + '" class="active" disabled="' + e.disabled + '">'
                            + '<span>' + e.title + '</span>'
                            + '</li>';
          } else {
            activeElement = '<li seperator="' + e.seperator + '" disabled="' + e.disabled + '">'
                            + '<span>' + e.title + '</span>'
                            + '</li>';
          }
          return activeElement;
        }).join('');
      $('.pagination li:not(.active) span').on('click', this.render.bind(this));

      // The organization table
      for (var i = (this.currentPage - 1) * ROWS_PER_PAGE;
        i < (this.currentPage * ROWS_PER_PAGE); i += 1) {
        var $rowContainer = $('<div class="row"></div>');
        if (this.groupedFilteredData[i] != null) {
          // eslint-disable-next-line no-loop-func
          this.groupedFilteredData[i].forEach(function(el) {
            var $columnContainer = $('<div class="col-xs-3 col-sm-3"></div>');
            $(el).appendTo($columnContainer);
            $columnContainer.appendTo($rowContainer);
          }, this);
          $rowContainer.appendTo(this.$orgContainer);
        }
      }
    };

    CustomerListHandler.prototype.getTotalPages = function() {
      return Math.ceil(this.groupedFilteredData.length / ROWS_PER_PAGE);
    };

    return CustomerListHandler;
  })();

  this.QuotedCustomersHighlights = (function() {
    function QuotedCustomersHighlights() {
      this.$quotedCustomers = $('.quoted-logo');
      this.$quoteContainer = $('.quote-container');
      this.$quoteAuthorContainer = $('.quote-author-container');
      this.$quoteAuthorChargeContainer = $('.quote-author-charge-container');
      this.$quotedCustomers.on('click', this.highlightClickedElement.bind(this));
      this.currentQuotedCustomerIndex = 0;
      this.timeoutQuotes = [];
      this.autoRotateQuote.call(this);
    }

    QuotedCustomersHighlights.prototype.highlightClickedElement = function(e) {
      if (e.originalEvent.isTrusted && this.timeoutQuotes.length > 0) {
        this.timeoutQuotes.forEach(function(el) {
          clearTimeout(el);
        });
        this.timeoutQuotes = [];
      }
      var $target = $(e.currentTarget);
      this.$quotedCustomers.removeClass('quoted-logo-clicked');
      this.$quoteContainer.text('"' + $target.data('quote') + '"');
      this.$quoteAuthorContainer.text($target.data('author'));
      var chargeUpcase = $target.data('author-charge') + ' at ' + $target.data('org-name');
      chargeUpcase = chargeUpcase.toUpperCase();
      this.$quoteAuthorChargeContainer.text(chargeUpcase);
      $target.addClass('quoted-logo-clicked');
    };

    QuotedCustomersHighlights.prototype.autoRotateQuote = function() {
      function rotate() {
        if (this.currentQuotedCustomerIndex > this.$quotedCustomers.length - 1) {
          this.currentQuotedCustomerIndex = 0;
        }
        this.$quotedCustomers[this.currentQuotedCustomerIndex].click({});
        this.currentQuotedCustomerIndex = this.currentQuotedCustomerIndex += 1;
        var that = this;
        that.timeoutQuotes.push(
          setTimeout(function() {
            rotate.call(that);
          }, 5000)
        );
      }

      rotate.call(this);
    };

    return QuotedCustomersHighlights;
  })();

  new QuotedCustomersHighlights();
  new CustomerListHandler();

  $('.js-open-list').on('click', function(e) {
    e.preventDefault();
    var isOpen = $(this).closest('.filter-list-container').is('.is-open');
    $('.filter-list-container.is-open').removeClass('is-open');

    if (!isOpen) {
      $(this).closest('.filter-list-container').toggleClass('is-open');
    }
  });

  var $ctaFormButton = $('button.cta-btn-addon');

  if ($ctaFormButton.length > 0) {
    addClickListenerCtaButton($ctaFormButton);
  }
})();
